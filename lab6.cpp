/**************************
 *Drew Garrison
 *CPSC 1021, Section 001, F20
 *awgarri@clemson.edu
 *Nustrat Humaira and John Choi
 **************************/
 
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
    string lastName;
    string firstName;
    int birthYear;
    double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
    // IMPLEMENT as instructed below
    /*This is to seed the random generator */
    srand(unsigned (time(0)));


    /*Create an array of 10 employees and fill information from standard input with prompt messages*/
    employee arr[10];
    for(int i=0; i<10; i++){
        cout<<"Employee["<<i+1<<"]..."<<endl;
        cout<<"Enter employee's first name: "; cin>>arr[i].firstName;
        cout<<"Enter employee's last name: "; cin>>arr[i].lastName;
        cout<<"Enter employee's birth year: "; cin>>arr[i].birthYear;
        cout<<"Enter employee's hourly wages: "; cin>>arr[i].hourlyWage;
    }
    /*After the array is created and initialzed we call random_shuffle() see the
     *notes to determine the parameters to pass in.*/
    random_shuffle(arr, arr+10, myrandom);

    /*Build a smaller array of 5 employees from the first five cards of the array created
     *above*/
    employee shuffledEmp[5];
    for(int i=0; i<5; i++)
        shuffledEmp[i] = arr[i];

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
    sort(shuffledEmp, shuffledEmp+5, name_order);

    /*Now print the array below */

    for(int i=0; i<5; i++){
        cout<< right << setw(30) << (shuffledEmp[i].lastName + ", " + shuffledEmp[i].firstName) <<endl;
        cout<< right << setw(30) << shuffledEmp[i].birthYear<<endl;
        cout<< right << setw(30) << fixed << setprecision(2)<< shuffledEmp[i].hourlyWage<<endl;
    }

    return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
    return lhs.lastName < rhs.lastName;
}